#!python3

import os
import sys
import time

import mysql.connector
import datetime

# Pulling the command line parameters

description = sys.argv[1] if len(sys.argv) > 1 else None
query = sys.argv[2] if len(sys.argv) > 2 else None
expected = int(sys.argv[3]) if len(sys.argv) > 3 else 1
timeout = int(sys.argv[4]) if len(sys.argv) > 4 else 600

if description is None:
    raise Exception("Description can't be empty (1-st argument)")

if query is None:
    raise Exception("Query can't be empty (2-nd argument)")

if expected is None:
    raise Exception("Expected can't be empty (3-rd argument)")

if timeout is None:
    raise Exception("Timeout, sec can't be empty (4-th argument)")

# Displaying 'welcome' message
print(f"\r\nWait for DB {description}:")

# Initialize MySQL DB credentials from environment variables

db_user = "root"
db_password = os.getenv("MYSQL_ROOT_PASSWORD")
db_name = os.getenv("MYSQL_DATABASE")
db_instance = os.getenv("INSTANCE_IP")
db_port = 7005

if db_password is None:
    raise Exception("DB password can't be empty (MYSQL_ROOT_PASSWORD)")

if db_name is None:
    raise Exception("DB name can't be empty (MYSQL_DATABASE)")

if db_instance is None:
    raise Exception("DB instance IP can't be empty (INSTANCE_IP)")

# Display initialized values information

print()
print(f"DB instance: {db_instance}")
print(f"DB name: {db_name}")
print(f"DB user: {db_user}")
print(f"DB password: {'x' * len(db_password)}")
print()
print(f"Query: {query}")
print(f"Expected query result: {expected}")
print(f"Timeout: {timeout} sec")
print()

# Prepare DB connection config

db_config = {
    'user': db_user,
    'password': db_password,
    'host': f'{db_instance}',
    'database': db_name,
    'port': db_port,
    'auth_plugin': 'caching_sha2_password',
    'connection_timeout': 4
}

# Initialize  interval values

is_db_available = False
is_query_doable = False
is_waiting_done = False
start_time = datetime.datetime.now()
last_print_time = None
seconds_passed = 0
query_result = None
delay = 10

print("Connecting to DB...")
while seconds_passed < timeout and query_result != expected:
    try:
        conn = mysql.connector.connect(**db_config)
        if not is_db_available:
            print("Connected.\r\nRunning the test query...")
        is_db_available = True

        curr = conn.cursor()
        curr.execute('SELECT COUNT(*) FROM InternalLogs')
        query_result = curr.fetchone()[0]

        if not is_query_doable:
            print("The test query is doable.")
        is_query_doable = True

    except:
        time.sleep(delay)
    finally:
        seconds_passed = (datetime.datetime.now() - start_time).seconds
        if last_print_time is None or (datetime.datetime.now() - last_print_time).seconds >= 60:
            if is_db_available:
                if query_result:
                    print(f'The query result: {query_result}')
                last_print_time = datetime.datetime.now()
            else:
                print(f"Awaiting for the DB availability...")
                last_print_time = datetime.datetime.now()

            print(f"Time elapsed: {seconds_passed}, sec")

        # If the condition doesn't match - sleep more
        if query_result != expected:
            time.sleep(delay)

# If the wait is finished and the condition is met, exit with code 0
if query_result == expected:
    print("\r\nWait is finished.")
    print(f"Time elapsed: {seconds_passed}, sec\r\n")
# If the wait is finished but the condition wasn't met, exit with the error code 1
else:
    print("\r\nWait is stopped due to the timeout.")
    sys.exit(1)
